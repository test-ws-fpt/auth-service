package injector

import (
	"ems/auth-service/facade"
	"github.com/google/wire"
)

var InjectorSet = wire.NewSet(wire.Struct(new(Injector), "*"))


type Injector struct {
	UserFacade facade.IUserFacade
	UserAssignEventRoleFacade facade.IUserAssignEventRoleFacade
	EventRolePermissionFacade facade.IEventRolePermissionFacade
	RolePermissionFacade facade.IRolePermissionFacade
	UserAssignRoleFacade facade.IUserAssignRoleFacade
}