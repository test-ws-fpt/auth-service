package impl

import "github.com/google/wire"

var ServiceSet = wire.NewSet(
	UserServiceSet,
	UserAssignEventRoleServiceSet,
	RolePermissionServiceSet,
	EventRolePermissionServiceSet,
	UserAssignRoleServiceSet,
)
