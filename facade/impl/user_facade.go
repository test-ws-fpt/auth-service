package impl

import (
	"context"
	"ems/auth-service/facade"
	"ems/auth-service/model"
	"ems/auth-service/populator"
	"ems/auth-service/service"
	emsError "ems/shared/error"
	auth "ems/shared/proto/auth"
	otpPB "ems/shared/proto/otp"
	"errors"
	"github.com/google/wire"
	httpError "github.com/micro/go-micro/v2/errors"
	log "github.com/micro/go-micro/v2/logger"
)

type UserFacade struct {
	UserService service.IUserService
	UserAssignRoleService service.IUserAssignRoleService
}


var _ facade.IUserFacade = (*UserFacade)(nil)

var UserFacadeSet = wire.NewSet(wire.Struct(new(UserFacade), "*"), wire.Bind(new(facade.IUserFacade), new(*UserFacade)))


func (u *UserFacade) ValidateRegister(username, phone, email string) (bool, bool, bool, error) {
	return u.UserService.ValidateRegister(username, phone, email)
}

func (u *UserFacade) GetEventRunners(tokenStr string) (*auth.GetEventRunnersResponse, error) {
	userAssignRoles, err := u.UserAssignRoleService.GetEventRunners(tokenStr)
	if err != nil {
		return nil, err
	}
	res := populator.GetEventRunnersPopulator(userAssignRoles)
	return res, nil
}

func (u *UserFacade) GetUserByAccessToken(tokenStr string) (*model.User, error) {
	return u.UserService.GetUserByAccessToken(tokenStr)
}

func (u *UserFacade) Register(ctx context.Context, otpSrvClient otpPB.OtpService, username, password, fullName, phone, email string, gender bool, emailOtpCode string) (*model.User, error) {
	res, err := otpSrvClient.ConfirmEmailOtp(ctx, &otpPB.ConfirmEmailOtpRequest{Code: &emailOtpCode, Email: &email})
	if err != nil {
		re, ok := err.(*httpError.Error)
		if ok {
			return nil, &emsError.ResultError{ErrorCode: int(re.Code), Err: errors.New(re.Error())}
		} else {
			log.Info(err)
			return nil, &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when confirming otp code")}
		}
	}
	if *res.Success {
		return u.UserService.Register(username, password, fullName, phone, email, gender)
	} else {
		return nil, &emsError.ResultError{ErrorCode: 400, Err: errors.New("otp code mismatched")}
	}

}

func (u *UserFacade) Login(username, password string, roleGroup uint32) (string, error) {
	return u.UserService.Login(username, password, roleGroup)
}


func (u *UserFacade) GetUserById(userId uint32) (*model.User, error) {
	return u.UserService.GetUserById(userId)
}