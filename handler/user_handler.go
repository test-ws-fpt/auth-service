package handler

import (
	"context"
	"ems/auth-service/facade"
	"ems/auth-service/validator"
	emsError "ems/shared/error"
	auth "ems/shared/proto/auth"
	otpPB "ems/shared/proto/otp"
	httpError "github.com/micro/go-micro/v2/errors"
	log "github.com/micro/go-micro/v2/logger"
	"github.com/micro/go-micro/v2/metadata"
	"strings"
)

type UserHandler struct {
	UserFacade facade.IUserFacade
	UserAssignRoleFacade facade.IUserAssignRoleFacade
	id		   string
	otpSrvClient otpPB.OtpService
}



func NewUserHandler(UserFacade facade.IUserFacade, UserAssignRoleFacade facade.IUserAssignRoleFacade, otpSrvClient otpPB.OtpService) *UserHandler {
	return &UserHandler{
		UserFacade: UserFacade,
		id:         "go.micro.service.auth",
		otpSrvClient: otpSrvClient,
		UserAssignRoleFacade: UserAssignRoleFacade,
	}
}

func (u *UserHandler) GetUserName(ctx context.Context, request *auth.GetUserNameRequest, response *auth.GetUserNameResponse) error {
	log.Info("Received Get User Name Request")
	user, err := u.UserFacade.GetUserById(*request.UserId)
	if err != nil {
		return EmsErrorToHttpError(err)
	}
	name := user.FullName
	email := user.Email
	response.Name = &name
	response.Email = &email
	return nil
}

func (u *UserHandler) WhoAmI(ctx context.Context, request *auth.WhoAmIRequest, response *auth.WhoAmIResponse) error {
	log.Info("Received Who Am I Request")
	authorization, ok := metadata.Get(ctx, "Authorization")
	if !ok {
		return httpError.Unauthorized("go.micro.service.auth", "no authorization received")
	}
	tokenStr := strings.Split(authorization, " ")[1]
	user, err := u.UserFacade.GetUserByAccessToken(tokenStr)
	if err != nil {
		return EmsErrorToHttpError(err)
	}
	userId := uint32(user.ID)
	organizationId := uint32(user.OrganizationId)
	name := user.FullName
	response.UserId = &userId
	response.OrganizationId = &organizationId
	response.Name = &name
	userAssignRole, err := u.UserAssignRoleFacade.GetUserRole(userId)
	if err != nil {
		return EmsErrorToHttpError(err)
	}
	role := uint32(userAssignRole.RoleId)
	response.RoleId = &role
	return nil
}

func (u *UserHandler) GetEventRunners(ctx context.Context, request *auth.GetEventRunnersRequest, response *auth.GetEventRunnersResponse) error {
	log.Info("Received Get Event Runners Request")
	authorization, ok := metadata.Get(ctx, "Authorization")
	if !ok {
		return httpError.Unauthorized("go.micro.service.auth", "no authorization received")
	}
	tokenStr := strings.Split(authorization, " ")[1]
	res, err := u.UserFacade.GetEventRunners(tokenStr)
	if err != nil {
		return EmsErrorToHttpError(err)
	}
	response.EventRunners = res.EventRunners
	return nil
}

func (u *UserHandler) ValidateRegister(ctx context.Context, request *auth.ValidateRegisterRequest, response *auth.ValidateRegisterResponse) error {
	log.Info("Received validate register request")
	er := validator.ValidateRegisterRequest(request)
	if er != nil {
		re := er.(*emsError.ResultError)
		return httpError.BadRequest(u.id, re.Err.Error())
	}
	validUsername, validPhone, validEmail, err := u.UserFacade.ValidateRegister(*request.Username, *request.Phone, *request.Email)
	if err != nil {
		return httpError.InternalServerError(u.id, "error when validating register")
	}
	response.ValidEmail = &validEmail
	response.ValidPhone = &validPhone
	response.ValidUsername = &validUsername
	return nil
}

func (u *UserHandler) Register(ctx context.Context, req *auth.RegisterRequest, res *auth.RegisterResponse) error {
	log.Info("Register request")
	er := validator.ValidateRegister(req)
	if er != nil {
		re := er.(*emsError.ResultError)
		return httpError.BadRequest(u.id,re.Err.Error())
	}
	_, err := u.UserFacade.Register(ctx, u.otpSrvClient, *req.Username, *req.Password, *req.FullName, *req.Phone, *req.Email, *req.Gender, *req.EmailOtpCode)
	if err == nil {
		var success = true
		res.Success = &success
		return nil
	} else {
		re, ok := err.(*emsError.ResultError)
		if ok {
			if re.ErrorCode == 500 {
				return httpError.InternalServerError(u.id, "error when registering a new account")
			} else {
				return httpError.BadRequest(u.id, re.Err.Error())
			}
		} else {
			return httpError.InternalServerError(u.id, "error when registering a new account")
		}

	}
}

func (u *UserHandler) Login(ctx context.Context, req *auth.LoginRequest, res *auth.LoginResponse) error {
	log.Info("Login request")
	er := validator.ValidateLogin(req)
	if er != nil {
		re := er.(*emsError.ResultError)
		return httpError.BadRequest(u.id,re.Err.Error())
	}
	token, err := u.UserFacade.Login(*req.Username, *req.Password, *req.RoleGroup)
	if err != nil {
		re, ok := err.(*emsError.ResultError)
		if ok {
			if re.ErrorCode == 500 {
				return httpError.InternalServerError(u.id, "error when logging in")
			} else {
				return httpError.BadRequest(u.id, re.Err.Error())
			}
		} else {
			return httpError.InternalServerError(u.id, "error when logging in")
		}
	} else {
		var success = true
		res.Success = &success
		res.AccessToken = &token
		return nil
	}
}

