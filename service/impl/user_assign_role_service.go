package impl

import (
	"ems/auth-service/model"
	"ems/auth-service/repository"
	"ems/auth-service/service"
	"github.com/google/wire"
)


var _ service.IUserAssignRoleService = (*UserAssignRoleService)(nil)

var UserAssignRoleServiceSet = wire.NewSet(wire.Struct(new(UserAssignRoleService), "*"), wire.Bind(new(service.IUserAssignRoleService), new(*UserAssignRoleService)))

type UserAssignRoleService struct {
	UserAssignRoleRepository repository.IUserAssignRoleRepository
	UserRepository repository.IUserRepository
}

func (u *UserAssignRoleService) GetUserRole(userId uint32) (*model.UserAssignRole, error) {
	return u.UserAssignRoleRepository.GetUserRoleByUserId(uint(userId))
}

func (u *UserAssignRoleService) GetEventRunners(tokenStr string) (*[]model.UserAssignRole, error) {
	userId, err := getUserIdFromAccessToken(tokenStr)
	if err != nil {
		return nil, err
	}
	user, err := u.UserRepository.GetUserById(userId)
	if err != nil {
		return nil, err
	}
	return u.UserAssignRoleRepository.GetEventRunners(user.OrganizationId)
}
