package impl

import "github.com/google/wire"

var FacadeSet = wire.NewSet(
	UserFacadeSet,
	UserAssignEventRoleFacadeSet,
	EventRolePermissionFacadeSet,
	RolePermissionFacadeSet,
	UserAssignRoleFacadeSet,
	)

