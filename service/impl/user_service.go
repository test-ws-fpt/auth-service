package impl

import (
	"ems/auth-service/model"
	"ems/auth-service/repository"
	"ems/auth-service/service"
	utils "ems/auth-service/utils"
	emsError "ems/shared/error"
	"errors"
	"github.com/google/wire"
	log "github.com/micro/go-micro/v2/logger"
)


var _ service.IUserService = (*UserService)(nil)

var UserServiceSet = wire.NewSet(wire.Struct(new(UserService), "*"), wire.Bind(new(service.IUserService), new(*UserService)))

type UserService struct {
	UserRepository repository.IUserRepository
	OrganizationRepository repository.IOrganizationRepository
	UserAssignRoleRepository repository.IUserAssignRoleRepository
}

func (s *UserService) GetUserById(userId uint32) (*model.User, error) {
	return s.UserRepository.GetUserById(uint(userId))
}

func (s *UserService) GetUserByAccessToken(tokenStr string) (*model.User, error) {
	userId, err := getUserIdFromAccessToken(tokenStr)
	if err != nil {
		return nil, err
	}
	return s.UserRepository.GetUserById(userId)
}

func (s *UserService) ValidateRegister(username, phone, email string) (bool, bool, bool, error) {
	validUsername, err := checkExistedUsername(s, username)
	if err != nil {
		return false, false, false, err
	}
	validPhone, err := checkExistedPhone(s, phone)
	if err != nil{
		return false, false, false, err
	}
	validEmail, err := checkExistedEmail(s, email)
	if err != nil {
		return false, false, false, err
	}
	if !validEmail {
		return validUsername, validPhone, validEmail, nil
	} else {
		domain, err := utils.GetDomainFromEmailAddress(email)
		if err != nil {
			log.Info(err.Error())
			return false, false, false, err
		}
		_, err = s.OrganizationRepository.GetOrganizationByDomainName(domain)
		if err != nil {
			re, ok := err.(*emsError.ResultError)
			if ok {
				if re.ErrorCode == 500 {
					return false, false, false, err
				} else {
					validEmail = false
				}
			} else {
				return false, false, false, err
			}
		}
		return validUsername, validPhone, validEmail, nil
	}
}

func (s *UserService) Register(username, password, fullName, phone, email string, gender bool) (*model.User, error) {
	log.Info("Register service")
	validUsername, validPhone, validEmail, err := s.ValidateRegister(username, phone, email)
	if !validUsername || !validPhone || !validEmail {
		return nil, &emsError.ResultError{ErrorCode: 400, Err: errors.New("username, email or phone invalid")}
	}
	if err != nil {
		return nil, err
	}
	domain, err := utils.GetDomainFromEmailAddress(email)
	if err != nil {
		log.Info(err.Error())
		return nil, err
	}
	organization, err := s.OrganizationRepository.GetOrganizationByDomainName(domain)
	if err != nil {
		return nil, err
	}
	u := model.User{
		Username: username,
		Phone:    phone,
		FullName: fullName,
		Email: 	  email,
		Gender:   gender,
		OrganizationId: organization.ID,
		Organization: *organization,
	}
	u.Password, err = utils.HashPassword(password)
	if err != nil {
		log.Info(err.Error())
		return nil, &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when creating new user")}
	}
	user, err := s.UserRepository.Register(&u)
	if err != nil {
		return nil, err
	}
	userAssignRole := model.UserAssignRole{
		UserId: user.ID,
		IsActive: true,
		RoleId: 3,
	}
	_, err = s.UserAssignRoleRepository.AddUserRole(&userAssignRole)
	if err != nil {
		return nil, err
	}
	return user, nil
}

func (s *UserService) Login(username, password string, roleGroup uint32) (string, error) {
	u := model.User{
		Username:   username,
		Password: 	password,
	}
	user, err := s.UserRepository.Login(&u)
	if err != nil {
		return "", err
	}
	role, err := s.UserAssignRoleRepository.GetUserRoleByUserId(user.ID)
	if err != nil {
		return "", err
	}
	if roleGroup == 1 {
		if role.RoleId != 1 && role.RoleId != 2 {
			return "", &emsError.ResultError{
				ErrorCode: 400,
				Err:       errors.New("this user and role do not match"),
			}
		}
	} else if roleGroup == 2 {
		if role.RoleId != 3 {
			return "", &emsError.ResultError{
				ErrorCode: 400,
				Err:       errors.New("this user and role do not match"),
			}
		}
	} else {
		return "", &emsError.ResultError{
			ErrorCode: 400,
			Err:       errors.New("invalid role"),
		}
	}
	return utils.GenerateToken(user.ID, user.OrganizationId, user.Username)
}

func checkExistedUsername(s *UserService, username string) (bool, error) {
	_, err := s.UserRepository.GetUserByUsername(username)
	if err == nil {
		return false, nil
	} else {
		re, ok := err.(*emsError.ResultError)
		if ok {
			if re.ErrorCode == 500 {
				return false, err
			}
		} else {
			return false, err
		}
	}
	return true, nil
}

func checkExistedPhone(s *UserService, phone string) (bool, error) {
	_, err := s.UserRepository.GetUserByPhone(phone)
	if err == nil {
		return false, nil
	} else {
		re, ok := err.(*emsError.ResultError)
		if ok {
			if re.ErrorCode == 500 {
				return false, err
			}
		} else {
			return false, err
		}
	}
	return true, nil
}

func checkExistedEmail(s *UserService, email string) (bool, error) {
	_, err := s.UserRepository.GetUserByEmail(email)
	if err == nil {
		return false, nil
	} else {
		re, ok := err.(*emsError.ResultError)
		if ok {
			if re.ErrorCode == 500 {
				return false, err
			}
		} else {
			return false, err
		}
	}
	return true, nil
}

