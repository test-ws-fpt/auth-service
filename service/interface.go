package service

import (
	"ems/auth-service/model"
)

type IUserService interface {
	Login(username, password string, roleGroup uint32) (string, error)
	Register(username, password, fullName, phone, email string, gender bool) (*model.User, error)
	ValidateRegister(username, phone, email string) (bool, bool, bool, error)
	GetUserByAccessToken(tokenStr string) (*model.User, error)
	GetUserById(userId uint32) (*model.User, error)
}

type IUserAssignEventRoleService interface {
	GetEventsByAccessToken(tokenStr string) (*[]model.UserAssignEventRole, error)
	GetEventMembersByEventId(eventId uint32) (*[]model.UserAssignEventRole, error)
	AddUserEventRole(userId, eventRoleId, eventId, organizationId uint32) error
}

type IEventRolePermissionService interface {
	CheckEventRolePermission(tokenStr string, PermissionId, eventId uint32) (bool, error)
}

type IRolePermissionService interface {
	CheckRolePermission(tokenStr string, PermissionId uint32) (bool, error)
}

type IUserAssignRoleService interface {
	GetEventRunners(tokenStr string) (*[]model.UserAssignRole, error)
	GetUserRole(userId uint32) (*model.UserAssignRole, error)
}