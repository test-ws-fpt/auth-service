package mysql

import (
	"ems/auth-service/model"
	repo "ems/auth-service/repository"
	emsError "ems/shared/error"
	"errors"
	"github.com/google/wire"
	"github.com/jinzhu/gorm"
	log "github.com/micro/go-micro/v2/logger"
)

var _ repo.IRolePermissionRepository = (*RolePermissionRepository)(nil)

var RolePermissionRepositorySet = wire.NewSet(wire.Struct(new(RolePermissionRepository), "*"), wire.Bind(new(repo.IRolePermissionRepository), new(*RolePermissionRepository)))

type RolePermissionRepository struct {
	DB *gorm.DB
}

func (e *RolePermissionRepository) CheckRolePermission(RoleId, PermissionId uint) (bool, error) {
	var rolePermission model.RolePermission
	if res := e.DB.Where("role_id = ? AND permission_id = ?", RoleId, PermissionId).Find(&rolePermission); res.Error != nil {
		log.Info(res.Error.Error())
		if !gorm.IsRecordNotFoundError(res.Error) {
			return false, &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when getting role permission")}
		} else {
			return false, nil
		}
	}
	return true, nil
}
