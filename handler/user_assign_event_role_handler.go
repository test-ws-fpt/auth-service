package handler

import (
	"context"
	"ems/auth-service/facade"
	auth "ems/shared/proto/auth"
	httpError "github.com/micro/go-micro/v2/errors"
	log "github.com/micro/go-micro/v2/logger"
	"github.com/micro/go-micro/v2/metadata"
	"strings"
)

type UserAssignEventRoleHandler struct {
	UserAssignEventRoleFacade facade.IUserAssignEventRoleFacade
	RolePermissionFacade facade.IRolePermissionFacade
	id		   string
}

func NewUserAssignEventRoleHandler(UserAssignEventRoleFacade facade.IUserAssignEventRoleFacade, RolePermissionFacade facade.IRolePermissionFacade) *UserAssignEventRoleHandler {
	return &UserAssignEventRoleHandler{
		UserAssignEventRoleFacade: UserAssignEventRoleFacade,
		RolePermissionFacade: RolePermissionFacade,
		id:         "go.micro.service.auth",
	}
}

func (u *UserAssignEventRoleHandler) AddEventMember(ctx context.Context, request *auth.AddEventMemberRequest, response *auth.AddEventMemberResponse) error {
	log.Info("Received Add Event Member Request")
	authorization, ok := metadata.Get(ctx, "Authorization")
	if !ok {
		return httpError.Unauthorized(u.id, "no authorization received")
	}
	tokenStr := strings.Split(authorization, " ")[1]
	check, err := u.RolePermissionFacade.CheckRolePermission(tokenStr, 23)
	if err != nil {
		return EmsErrorToHttpError(err)
	}
	if !check {
		return httpError.Forbidden(u.id, "not permitted")
	}

	for _, m := range request.EventMembers {
		err = u.UserAssignEventRoleFacade.AddUserEventRole(*m.Id, *m.EventRoleId, *request.EventId, *request.OrganizationId)
		if err != nil {
			return EmsErrorToHttpError(err)
		}
	}
	success := true
	response.Success = &success
	return nil
}

func (u *UserAssignEventRoleHandler) GetEventOperationLeaders(ctx context.Context, request *auth.GetEventOperationLeadersRequest, response *auth.GetEventOperationLeadersResponse) error {
	log.Info("Received Get Event Operation Leaders Request")
	authorization, ok := metadata.Get(ctx, "Authorization")
	if !ok {
		return httpError.Unauthorized(u.id, "no authorization received")
	}
	tokenStr := strings.Split(authorization, " ")[1]
	res, err := u.UserAssignEventRoleFacade.GetEventMembersByEventId(tokenStr, *request.EventId)
	if err != nil {
		return EmsErrorToHttpError(err)
	}
	response.OperationLeaders = res.OperationLeaders
	readOnly := true
	response.ReadOnly = &readOnly
	return nil
}

func (u *UserAssignEventRoleHandler) GetEventsByUserId(ctx context.Context, empty *auth.Empty, response *auth.GetEventsByUserIdResponse) error {
	log.Info("Received Get Events By User Id Request")
	authorization, ok := metadata.Get(ctx, "Authorization")
	if !ok {
		return httpError.Unauthorized(u.id, "no authorization received")
	}
	tokenStr := strings.Split(authorization, " ")[1]
	events, err := u.UserAssignEventRoleFacade.GetEventsByToken(tokenStr)
	if err != nil {
		return EmsErrorToHttpError(err)
	}
	response.Events = events.Events
	return nil

}
