package impl

import (
	"ems/auth-service/facade"
	"ems/auth-service/model"
	"ems/auth-service/service"
	"github.com/google/wire"
)

type UserAssignRoleFacade struct {
	UserAssignRoleService service.IUserAssignRoleService
}


var _ facade.IUserAssignRoleFacade = (*UserAssignRoleFacade)(nil)

var UserAssignRoleFacadeSet = wire.NewSet(wire.Struct(new(UserAssignRoleFacade), "*"), wire.Bind(new(facade.IUserAssignRoleFacade), new(*UserAssignRoleFacade)))

func (u *UserAssignRoleFacade) GetUserRole(userId uint32) (*model.UserAssignRole, error) {
	return u.UserAssignRoleService.GetUserRole(userId)
}
