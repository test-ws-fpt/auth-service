package mysql

import (
	"ems/auth-service/model"
	repo "ems/auth-service/repository"
	emsError "ems/shared/error"
	"errors"
	"github.com/google/wire"
	"github.com/jinzhu/gorm"
	log "github.com/micro/go-micro/v2/logger"
)

var _ repo.IEventRolePermissionRepository = (*EventRolePermissionRepository)(nil)

var EventRolePermissionRepositorySet = wire.NewSet(wire.Struct(new(EventRolePermissionRepository), "*"), wire.Bind(new(repo.IEventRolePermissionRepository), new(*EventRolePermissionRepository)))

type EventRolePermissionRepository struct {
	DB *gorm.DB
}

func (e *EventRolePermissionRepository) CheckEventRolePermission(EventRoleId, PermissionId uint) (bool, error) {
	var eventRolePermission model.EventRolePermission
	if res := e.DB.Where("event_role_id = ? AND permission_id = ?", EventRoleId, PermissionId).First(&eventRolePermission); res.Error != nil {
		log.Info(res.Error.Error())
		if !gorm.IsRecordNotFoundError(res.Error) {
			return false, &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when getting event role permission")}
		} else {
			return false, nil
		}
	}
	return true, nil
}
