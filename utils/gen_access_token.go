package utils

import (
	emsError "ems/shared/error"
	"errors"
	"github.com/dgrijalva/jwt-go"
	log "github.com/micro/go-micro/v2/logger"
	"io/ioutil"
	"time"
)


//generate access token from rsa key
func GenerateToken(userId, organizationId uint, name string) (string, error) {
	urlPrivateKey := "key/id.rsa"
	keyData, err := ioutil.ReadFile(urlPrivateKey)
	if err != nil {
		log.Info(err.Error())
		return "", &emsError.ResultError{ErrorCode: 500, Err: errors.New("failed to read private key file")}
	}
	key, err := jwt.ParseRSAPrivateKeyFromPEM(keyData)
	if err != nil {
		log.Info(err.Error())
		return "", &emsError.ResultError{ErrorCode: 500, Err: errors.New("failed to parse data from private key file")}
	}
	token := jwt.New(jwt.SigningMethodRS256)
	claims := token.Claims.(jwt.MapClaims)
	claims["userId"] = userId
	claims["organizationId"] = organizationId
	claims["name"] = name
	claims["iat"] = time.Now()
	claims["exp"] = time.Now().Add(time.Minute * 60 * 24 * 365).Unix()
	priv, err := token.SignedString(key)
	if err != nil {
		log.Info(err.Error())
		return "", &emsError.ResultError{ErrorCode: 500, Err: errors.New("failed to sign key")}
	}
	return priv, nil
}
