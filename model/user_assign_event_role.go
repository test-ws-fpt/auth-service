package model

type UserAssignEventRole struct {
	ID       	uint `gorm:"primary_key;auto_increment"`
	UserId 			uint
	User 			User
	EventRoleId 	uint
	EventRole 		EventRole
	EventId 		uint

}

func (UserAssignEventRole) TableName() string {
	return "user_assign_event_role"
}
