package mysql

import (
	"ems/auth-service/model"
	repo "ems/auth-service/repository"
	emsError "ems/shared/error"
	"errors"
	"github.com/google/wire"
	"github.com/jinzhu/gorm"
	log "github.com/micro/go-micro/v2/logger"
)

var _ repo.IOrganizationRepository = (*OrganizationRepository)(nil)

var OrganizationRepositorySet = wire.NewSet(wire.Struct(new(OrganizationRepository), "*"), wire.Bind(new(repo.IOrganizationRepository), new(*OrganizationRepository)))

type OrganizationRepository struct {
	DB *gorm.DB
}

func (repo *OrganizationRepository) GetOrganizationByDomainName(domainName string) (*model.Organization, error) {
	organization := model.Organization{}
	if res := repo.DB.Where("domain_name = ?", domainName).First(&organization);  res.Error != nil {
		log.Info(res.Error.Error())
		if gorm.IsRecordNotFoundError(res.Error) {
			return nil, &emsError.ResultError{ErrorCode: 400, Err: errors.New("organization does not exist")}
		} else {
			return nil, &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when getting organization")}
		}
	}
	return &organization, nil
}

