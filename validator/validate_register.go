package validator

import (
	emsError "ems/shared/error"
	auth "ems/shared/proto/auth"
	"regexp"
	"strings"
	"errors"
)

func ValidateRegister(req *auth.RegisterRequest) error {
	re := regexp.MustCompile(`^0[0-9]{9}$`)
	reg := regexp.MustCompile(`^[0-9]{6}$`)
	if strings.TrimSpace(*req.Username) == "" {
		return &emsError.ResultError{ErrorCode: 400, Err: errors.New("invalid username")}
	}
	if strings.TrimSpace(*req.Password) == "" {
		return &emsError.ResultError{ErrorCode: 400, Err: errors.New("invalid password")}
	}
	if strings.TrimSpace(*req.FullName) == "" {
		return &emsError.ResultError{ErrorCode: 400, Err: errors.New("invalid full name")}
	}
	if !reg.MatchString(*req.EmailOtpCode) {
		return &emsError.ResultError{ErrorCode: 400, Err: errors.New("invalid email otp code")}
	}
	if !re.MatchString(*req.Phone)  {
		return &emsError.ResultError{ErrorCode: 400, Err: errors.New("invalid phone number")}
	}
	at := strings.LastIndex(*req.Email, "@")
	if at < 0 {
		return &emsError.ResultError{ErrorCode: 400, Err: errors.New("invalid email address")}
	}

	return nil
}