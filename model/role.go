package model

type Role struct {
	ID       	uint `gorm:"primary_key;auto_increment"`
	Name    		string
	Description 	string
	RoleGroupId		uint
	RoleGroup 		RoleGroup
	UserAssignRoles []UserAssignRole
	RolePermissions []RolePermission

}

func (Role) TableName() string {
	return "role"
}
