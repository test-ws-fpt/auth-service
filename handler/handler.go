package handler

import (
	"ems/auth-service/injector"
	emsError "ems/shared/error"
	auth "ems/shared/proto/auth"
	otpPB "ems/shared/proto/otp"
	httpError "github.com/micro/go-micro/v2/errors"
	log "github.com/micro/go-micro/v2/logger"
	"github.com/micro/go-micro/v2/server"
)

func Apply1(server server.Server, injector injector.Injector, otpSrvClient otpPB.OtpService) {
	auth.RegisterUserHandler(server, NewUserHandler(injector.UserFacade, injector.UserAssignRoleFacade, otpSrvClient))
	auth.RegisterUserAssignEventRoleHandler(server, NewUserAssignEventRoleHandler(injector.UserAssignEventRoleFacade, injector.RolePermissionFacade))
	auth.RegisterEventRolePermissionHandler(server, NewEventRolePermissionHandler(injector.EventRolePermissionFacade))
	auth.RegisterRolePermissionHandler(server, NewRolePermissionHandler(injector.RolePermissionFacade))
}

func EmsErrorToHttpError (err error) error {
	log.Info(err.Error())
	re, ok := err.(*emsError.ResultError)
	if ok {
		if re.ErrorCode == 400 {
			return httpError.BadRequest("go.micro.service.auth", re.Err.Error())
		} else if re.ErrorCode == 401 {
			return httpError.Unauthorized("go.micro.service.auth", re.Err.Error())
		} else if re.ErrorCode == 403 {
			return httpError.Forbidden("go.micro.service.auth", re.Err.Error())
		} else if re.ErrorCode == 404 {
			return httpError.NotFound("go.micro.service.auth", re.Err.Error())
		} else {
			return httpError.InternalServerError("go.micro.service.auth", re.Err.Error())
		}
	} else {
		return httpError.InternalServerError("go.micro.service.auth", "unknown server error")
	}
}