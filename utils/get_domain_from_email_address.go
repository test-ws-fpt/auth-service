package utils

import (
	emsError "ems/shared/error"
	"errors"
	"strings"
)

//get email address domain
func GetDomainFromEmailAddress(email string) (string, error){
	at := strings.LastIndex(email, "@")
	if at >= 0 {
		domain := email[at+1:]
		return domain, nil
	} else {
		return "", &emsError.ResultError{ErrorCode: 400, Err: errors.New("invalid email address")}
	}
}