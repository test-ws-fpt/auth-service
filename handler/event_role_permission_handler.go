package handler

import (
	"context"
	"ems/auth-service/enum"
	"ems/auth-service/facade"
	"ems/auth-service/utils"
	auth "ems/shared/proto/auth"
	"fmt"
	httpError "github.com/micro/go-micro/v2/errors"
	log "github.com/micro/go-micro/v2/logger"
	"github.com/micro/go-micro/v2/metadata"
	"reflect"
	"strings"
)

type EventRolePermissionHandler struct {
	EventRolePermissionFacade facade.IEventRolePermissionFacade
	id		   string
}


func NewEventRolePermissionHandler(EventRolePermissionFacade facade.IEventRolePermissionFacade) *EventRolePermissionHandler {
	return &EventRolePermissionHandler{
		EventRolePermissionFacade: EventRolePermissionFacade,
		id:         "go.micro.service.auth",
	}
}


func (e *EventRolePermissionHandler) GetUserEventPermission(ctx context.Context, request *auth.GetUserEventPermissionRequest, response *auth.GetUserEventPermissionResponse) error {
	log.Info("Received Get User Event Permission Request")
	authorization, ok := metadata.Get(ctx, "Authorization")
	if !ok {
		return httpError.Unauthorized("go.micro.service.auth", "no authorization received")
	}
	tokenStr := strings.Split(authorization, " ")[1]
	r := reflect.ValueOf(request).Elem()
	for i :=0; i< r.NumField(); i++ {
		if r.Field(i).CanSet() && strings.Compare(fmt.Sprint(reflect.Indirect(r.Field(i))), "true") == 0 {
			check, err := e.EventRolePermissionFacade.CheckEventRolePermission(tokenStr, enum.EventPermissionCodeToId(r.Type().Field(i).Name), *request.EventId)
			if err != nil {
				return EmsErrorToHttpError(err)
			}
			err = utils.SetBoolValue(response, r.Type().Field(i).Name, check)
			if err != nil {
				log.Info(err.Error())
				return EmsErrorToHttpError(err)
			}
		}
	}

	return nil
}