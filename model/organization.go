package model

type Organization struct {
	ID       	uint `gorm:"primary_key;auto_increment"`
	Name	    string
	DomainName 	string
	Address		string
	Phone       string
	Email 		string
	Users 		[]User
}

func (Organization) TableName() string {
	return "organization"
}