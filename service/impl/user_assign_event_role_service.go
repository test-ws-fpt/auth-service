package impl

import (
	"ems/auth-service/model"
	"ems/auth-service/repository"
	"ems/auth-service/service"
	emsError "ems/shared/error"
	"errors"
	"github.com/google/wire"
)


var _ service.IUserAssignEventRoleService = (*UserAssignEventRoleService)(nil)

var UserAssignEventRoleServiceSet = wire.NewSet(wire.Struct(new(UserAssignEventRoleService), "*"), wire.Bind(new(service.IUserAssignEventRoleService), new(*UserAssignEventRoleService)))

type UserAssignEventRoleService struct {
	UserAssignEventRoleRepository repository.IUserAssignEventRoleRepository
	UserRepository repository.IUserRepository
}

func (u *UserAssignEventRoleService) AddUserEventRole(userId, eventRoleId, eventId, organizationId uint32) error {
	user, err := u.UserRepository.GetUserById(uint(userId))
	if err != nil {
		return err
	}
	if user.OrganizationId != uint(organizationId) {
		return &emsError.ResultError{
			ErrorCode: 400,
			Err:       errors.New("this user does not belong to the organization"),
		}
	}
	userAssignEventRole := model.UserAssignEventRole{
		UserId:      uint(userId),
		EventRoleId: uint(eventRoleId),
		EventId: uint(eventId),
	}
	return u.UserAssignEventRoleRepository.AddUserEventRole(&userAssignEventRole)
}

func (u *UserAssignEventRoleService) GetEventsByAccessToken(tokenStr string) (*[]model.UserAssignEventRole, error) {
	userId, err := getUserIdFromAccessToken(tokenStr)
	if err != nil {
		return nil, err
	}
	return u.UserAssignEventRoleRepository.GetUserEventRolesByUserId(userId)
}

func (u *UserAssignEventRoleService) GetEventMembersByEventId(eventId uint32) (*[]model.UserAssignEventRole, error) {
	return u.UserAssignEventRoleRepository.GetEventMembersByEventId(uint(eventId))
}
