package populator

import (
	"ems/auth-service/model"
	auth "ems/shared/proto/auth"
)

func GetEventRunnersPopulator(userAssignRoles *[]model.UserAssignRole) *auth.GetEventRunnersResponse {
	var res auth.GetEventRunnersResponse
	for _, u := range *userAssignRoles {
		Id := uint32(u.User.ID)
		Name := u.User.FullName
		res.EventRunners = append(res.EventRunners, &auth.EventRunnerDict{
			Id:   &Id,
			Name: &Name,
		})
	}
	return &res
}
