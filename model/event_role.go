package model

type EventRole struct {
	ID       	uint `gorm:"primary_key;auto_increment"`
	Name    			string
	Description 		string
	EventRoleGroupId	uint
	EventRoleGroup 		EventRoleGroup
	UserAssignEventRoles []UserAssignEventRole
}

func (EventRole) TableName() string {
	return "event_role"
}
