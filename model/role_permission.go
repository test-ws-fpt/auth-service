package model

type RolePermission struct {
	ID       	uint `gorm:"primary_key;auto_increment"`
	PermissionId 	uint
	Permission 		Permission
	RoleId 			uint
	Role 			Role

}

func (RolePermission) TableName() string {
	return "role_permission"
}