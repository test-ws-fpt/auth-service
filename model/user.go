package model

import (
	"github.com/jinzhu/gorm"
)

type User struct {
	gorm.Model
	Username    string
	Password 	string
	FullName	string
	Gender      bool
	Phone       string
	Email 		string
	OrganizationId	uint
	Organization Organization
	UserAssignRoles []UserAssignRole
	UserAssignEventRoles []UserAssignEventRole

}

func (User) TableName() string {
	return "user"
}
