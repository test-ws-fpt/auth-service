package model

type UserAssignRole struct {
	ID       	uint `gorm:"primary_key;auto_increment"`
	UserId 		uint
	User 		User
	RoleId 		uint
	Role 		Role
	IsActive	bool

}

func (UserAssignRole) TableName() string {
	return "user_assign_role"
}


