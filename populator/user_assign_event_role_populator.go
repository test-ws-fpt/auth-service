package populator

import (
	"ems/auth-service/model"
	auth "ems/shared/proto/auth"
)

func GetEventOperationLeadersPopulator(userAssignEventRoles *[]model.UserAssignEventRole) *auth.GetEventOperationLeadersResponse {
	var res auth.GetEventOperationLeadersResponse
	for _, u := range *userAssignEventRoles {
		Id := uint32(u.UserId)
		Name := u.User.FullName
		Phone := u.User.Phone
		EventRole := uint32(u.EventRoleId)
		eold := auth.EventOperationLeaderDict{
			Id: &Id,
			Name: &Name,
			Phone: &Phone,
			EventRole: &EventRole,
		}
		res.OperationLeaders = append(res.OperationLeaders, &eold)
	}
	return &res
}

