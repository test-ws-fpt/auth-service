package mysql

import (
	"ems/auth-service/model"
	repo "ems/auth-service/repository"
	emsError "ems/shared/error"
	"errors"
	"github.com/google/wire"
	"github.com/jinzhu/gorm"
	log "github.com/micro/go-micro/v2/logger"
)

var _ repo.IUserAssignEventRoleRepository = (*UserAssignEventRoleRepository)(nil)

var UserAssignEventRoleRepositorySet = wire.NewSet(wire.Struct(new(UserAssignEventRoleRepository), "*"), wire.Bind(new(repo.IUserAssignEventRoleRepository), new(*UserAssignEventRoleRepository)))

type UserAssignEventRoleRepository struct {
	DB *gorm.DB
}

func (u *UserAssignEventRoleRepository) AddUserEventRole(userAssignEventRole *model.UserAssignEventRole) error {
	if res := u.DB.Create(&userAssignEventRole); res.Error != nil {
		log.Info(res.Error.Error())
		return &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when adding user to event")}
	}
	return nil
}

func (u *UserAssignEventRoleRepository) GetUserEventRoleByUserIdAndEventId(userId, eventId uint) (*model.UserAssignEventRole, error) {
	var userAssignEventRoles model.UserAssignEventRole
	if res := u.DB.Where("user_id = ? AND event_id = ?", userId, eventId).Find(&userAssignEventRoles); res.Error != nil {
		if !gorm.IsRecordNotFoundError(res.Error) {
			log.Info(res.Error.Error())
			return nil, &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when getting user role")}
		} else {
			return nil, &emsError.ResultError{ErrorCode: 401, Err: errors.New("user is not a part of this event")}
		}
	}
	return &userAssignEventRoles, nil
}

func (u *UserAssignEventRoleRepository) GetEventMembersByEventId(eventId uint) (*[]model.UserAssignEventRole, error) {
	var userAssignEventRoles []model.UserAssignEventRole
	if res := u.DB.Preload("User").Where("event_id = ?", eventId).Find(&userAssignEventRoles); res.Error != nil {
		if gorm.IsRecordNotFoundError(res.Error) {
			return nil, &emsError.ResultError{ErrorCode: 400, Err: errors.New("event does not exist or does not have any members")}
		} else {
			log.Info(res.Error.Error())
			return nil, &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when getting event members")}
		}
	}
	return &userAssignEventRoles, nil
}

func (u *UserAssignEventRoleRepository) GetUserEventRolesByUserId(userId uint) (*[]model.UserAssignEventRole, error) {
	var userAssignEventRoles []model.UserAssignEventRole
	if res := u.DB.Where("user_id = ?", userId).Find(&userAssignEventRoles); res.Error != nil {
		if !gorm.IsRecordNotFoundError(res.Error) {
			log.Info(res.Error.Error())
			return nil, &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when getting user role")}
		}
	}
	return &userAssignEventRoles, nil
}






