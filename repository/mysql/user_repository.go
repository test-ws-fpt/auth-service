package mysql

import (
	"ems/auth-service/model"
	repo "ems/auth-service/repository"
	utils "ems/auth-service/utils"
	emsError "ems/shared/error"
	"errors"
	"github.com/google/wire"
	"github.com/jinzhu/gorm"
	log "github.com/micro/go-micro/v2/logger"
)

var _ repo.IUserRepository = (*UserRepository)(nil)

var UserRepositorySet = wire.NewSet(wire.Struct(new(UserRepository), "*"), wire.Bind(new(repo.IUserRepository), new(*UserRepository)))

type UserRepository struct {
	DB *gorm.DB
}

func (repo *UserRepository) GetUserById(id uint) (*model.User, error) {
	user := model.User{}
	if res := repo.DB.Where("id = ?", id).First(&user);  res.Error != nil {
		log.Info(res.Error.Error())
		if gorm.IsRecordNotFoundError(res.Error) {
			return nil, &emsError.ResultError{ErrorCode: 400, Err: errors.New("user does not exist")}
		} else {
			return nil, &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when getting user")}
		}
	}
	return &user, nil
}

func (repo *UserRepository) GetUserByUsername(username string) (*model.User, error) {
	user := model.User{}
	if res := repo.DB.Where("username = ?", username).First(&user);  res.Error != nil {
		log.Info(res.Error.Error())
		if gorm.IsRecordNotFoundError(res.Error) {
			return nil, &emsError.ResultError{ErrorCode: 400, Err: errors.New("user does not exist")}
		} else {
			return nil, &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when getting user")}
		}
	}
	return &user, nil
}

func (repo *UserRepository) GetUserByPhone(phone string) (*model.User, error) {
	user := model.User{}
	if res := repo.DB.Where("phone = ?", phone).First(&user);  res.Error != nil {
		log.Info(res.Error.Error())
		if gorm.IsRecordNotFoundError(res.Error) {
			return nil, &emsError.ResultError{ErrorCode: 400, Err: errors.New("user does not exist")}
		} else {
			return nil, &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when getting user")}
		}
	}
	return &user, nil
}

func (repo *UserRepository) GetUserByEmail(email string) (*model.User, error) {
	user := model.User{}
	if res := repo.DB.Where("email = ?", email).First(&user);  res.Error != nil {
		log.Info(res.Error.Error())
		if gorm.IsRecordNotFoundError(res.Error) {
			return nil, &emsError.ResultError{ErrorCode: 400, Err: errors.New("user does not exist")}
		} else {
			return nil, &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when getting user")}
		}
	}
	return &user, nil
}

func (repo *UserRepository) Login(user *model.User) (*model.User, error) {
	newUser, err := repo.GetUserByUsername(user.Username)
	if err != nil {
		return nil, err
	}
	if !utils.CheckPasswordHash(user.Password, newUser.Password) {
		return nil, &emsError.ResultError{ErrorCode: 400, Err: errors.New("invalid password")}
	}
	return newUser, nil
}

func (repo *UserRepository) Register(user *model.User) (*model.User, error) {
	res := repo.DB.Create(user)
	if res.Error != nil {
		log.Info(res.Error.Error())
		return nil, &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when creating new User")}
	}
	return repo.GetUserByUsername(user.Username)
}

