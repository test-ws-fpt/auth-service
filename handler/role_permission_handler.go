package handler

import (
	"context"
	"ems/auth-service/enum"
	"ems/auth-service/facade"
	"ems/auth-service/utils"
	auth "ems/shared/proto/auth"
	"fmt"
	httpError "github.com/micro/go-micro/v2/errors"
	log "github.com/micro/go-micro/v2/logger"
	"github.com/micro/go-micro/v2/metadata"
	"reflect"
	"strings"
)

type RolePermissionHandler struct {
	RolePermissionFacade facade.IRolePermissionFacade
}


func NewRolePermissionHandler(RolePermissionFacade facade.IRolePermissionFacade) *RolePermissionHandler {
	return &RolePermissionHandler{
		RolePermissionFacade: RolePermissionFacade,
	}
}


func (e *RolePermissionHandler) GetUserPermission(ctx context.Context, request *auth.GetUserPermissionRequest, response *auth.GetUserPermissionResponse) error {
	log.Info("Received Get User Permission Request")
	authorization, ok := metadata.Get(ctx, "Authorization")
	if !ok {
		return httpError.Unauthorized("go.micro.service.auth", "no authorization received")
	}
	tokenStr := strings.Split(authorization, " ")[1]
	r := reflect.ValueOf(request).Elem()
	for i :=0; i< r.NumField(); i++ {
		if r.Field(i).CanSet() && strings.Compare(fmt.Sprint(reflect.Indirect(r.Field(i))), "true") == 0 {
			check, err := e.RolePermissionFacade.CheckRolePermission(tokenStr, enum.PermissionCodeToId(r.Type().Field(i).Name))
			if err != nil {
				return EmsErrorToHttpError(err)
			}
			err = utils.SetBoolValue(response, r.Type().Field(i).Name, check)
			if err != nil {
				log.Info(err.Error())
				return EmsErrorToHttpError(err)
			}
		}
	}

	return nil
}