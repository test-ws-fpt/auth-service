package utils

import (
	"errors"
	"fmt"
	"reflect"
)


// set a property of a struct by name, but only if that property's type is bool
func SetBoolValue(v interface{}, name string, value bool) error {
	// v must be a pointer to a struct
	rv := reflect.ValueOf(v)
	if rv.Kind() != reflect.Ptr || rv.Elem().Kind() != reflect.Struct {
		return errors.New("v must be pointer to struct")
	}

	// Dereference pointer
	rv = rv.Elem()

	// Lookup field by name
	fv := rv.FieldByName(name)
	if !fv.IsValid() {
		return fmt.Errorf("not a field name: %s", name)
	}

	// Field must be exported
	if !fv.CanSet() {
		return fmt.Errorf("cannot set field %s", name)
	}

	// We expect a pointer field
	if fv.Kind() != reflect.Ptr {
		return fmt.Errorf("%s is not a Ptr field", name)
	}

	// Set the value
	fv.Set(reflect.ValueOf(&value))
	return nil
}
