package facade

import (
	"context"
	"ems/auth-service/model"
	auth "ems/shared/proto/auth"
	otpPB "ems/shared/proto/otp"
)

type IUserFacade interface {
	Login(username, password string, roleGroup uint32) (string, error)
	Register(ctx context.Context, otpSrvClient otpPB.OtpService, username, password, fullName, phone, email string, gender bool, emailOtpCode string) (*model.User, error)
	ValidateRegister(username, phone, email string) (bool, bool, bool, error)
	GetUserByAccessToken(tokenStr string) (*model.User, error)
	GetEventRunners(tokenStr string) (*auth.GetEventRunnersResponse, error)
	GetUserById(userId uint32) (*model.User, error)
}

type IEventRolePermissionFacade interface {
	CheckEventRolePermission(tokenStr string, PermissionId, eventId uint32) (bool, error)
}

type IRolePermissionFacade interface {
	CheckRolePermission(tokenStr string, PermissionId uint32) (bool, error)
}

type IUserAssignEventRoleFacade interface {
	GetEventMembersByEventId(tokenStr string, eventId uint32) (*auth.GetEventOperationLeadersResponse, error)
	GetEventsByToken(tokenStr string) (*auth.GetEventsByUserIdResponse, error)
	AddUserEventRole(userId, eventRoleId, eventId, organizationId uint32) error
}

type IUserAssignRoleFacade interface {
	GetUserRole(userId uint32) (*model.UserAssignRole, error)
}