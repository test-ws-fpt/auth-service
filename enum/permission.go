package enum


//convert permission string to id
func PermissionCodeToId(permissionCode string) uint32 {
	Permission := map[string]uint32 {
		"ViewAllApplications": 1,
		"ViewAllPendingApplications": 2,
		"ViewAllEvents": 3,
		"PreliminaryApproveApplication": 22,
		"FinalApproveApplication": 23,
		"ReviewEvent": 24,
		"AddApplication": 25,

	}
	return Permission[permissionCode]
}


//convert event permission string to id
func EventPermissionCodeToId(eventPermissionCode string) uint32 {
	EventPermission := map[string]uint32 {
		"ViewEventInvoices": 4,
		"ViewEventHumanResource": 5,
		"ViewEventContacts": 6,
		"ViewEventRisks": 7,
		"ViewAllEventTasks": 8,
		"AddEventMember": 9,
		"UpdateEventMember": 10,
		"DeleteEventMember": 11,
		"UpdateEventTask": 12,
		"DeleteEventTask": 13,
		"AddEventRisk": 14,
		"UpdateEventRisk": 15,
		"DeleteEventRisk": 16,
		"AddEventContact": 17,
		"UpdateEventContact": 18,
		"DeleteEventContact": 19,
		"AddOperationLeader": 20,
		"DeleteOperationLeader": 21,
		"ViewEventReview": 26,
		"AddEventTask": 27,

	}
	return EventPermission[eventPermissionCode]
}
