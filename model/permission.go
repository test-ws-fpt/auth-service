package model

type Permission struct {
	ID       	uint `gorm:"primary_key;auto_increment"`
	Code   			string
	Description 	string
	RolePermissions []RolePermission
}

func (Permission) TableName() string {
	return "permission"
}
