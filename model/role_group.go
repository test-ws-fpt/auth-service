package model

type RoleGroup struct {
	ID       	uint `gorm:"primary_key;auto_increment"`
	Name    		string
	Description 	string
	OrganizationId	uint
	Organization 	Organization

}

func (RoleGroup) TableName() string {
	return "role_group"
}
