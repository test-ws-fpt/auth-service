package repository

import (
	"ems/auth-service/model"
)

type IUserRepository interface {
	Login(user *model.User) (*model.User, error)
	Register(user *model.User) (*model.User, error)
	GetUserByUsername(username string) (*model.User, error)
	GetUserByPhone(phone string) (*model.User, error)
	GetUserByEmail(email string) (*model.User, error)
	GetUserById(id uint) (*model.User, error)
}

type IOrganizationRepository interface {
	GetOrganizationByDomainName(domainName string) (*model.Organization, error)
}

type IUserAssignRoleRepository interface {
	AddUserRole(userAssignRole *model.UserAssignRole) (*model.UserAssignRole, error)
	GetUserRoleByUserId(userId uint) (*model.UserAssignRole,error)
	GetEventRunners(organizationId uint) (*[]model.UserAssignRole, error)
}

type IUserAssignEventRoleRepository interface {
	GetUserEventRolesByUserId(userId uint) (*[]model.UserAssignEventRole, error)
	GetEventMembersByEventId(eventId uint) (*[]model.UserAssignEventRole, error)
	GetUserEventRoleByUserIdAndEventId(userId, eventId uint) (*model.UserAssignEventRole, error)
	AddUserEventRole(userAssignEventRole *model.UserAssignEventRole) error
}

type IRolePermissionRepository interface {
	CheckRolePermission(RoleId, PermissionId uint) (bool, error)
}

type IEventRolePermissionRepository interface {
	CheckEventRolePermission(EventRoleId, PermissionId uint) (bool, error)
}

