package impl

import (
	"ems/auth-service/facade"
	"ems/auth-service/populator"
	"ems/auth-service/service"
	auth "ems/shared/proto/auth"
	"github.com/google/wire"
)

type UserAssignEventRoleFacade struct {
	UserAssignEventRoleService service.IUserAssignEventRoleService
}
var _ facade.IUserAssignEventRoleFacade = (*UserAssignEventRoleFacade)(nil)

var UserAssignEventRoleFacadeSet = wire.NewSet(wire.Struct(new(UserAssignEventRoleFacade), "*"), wire.Bind(new(facade.IUserAssignEventRoleFacade), new(*UserAssignEventRoleFacade)))

func (u *UserAssignEventRoleFacade) AddUserEventRole(userId, eventRoleId, eventId, organizationId uint32) error {
	return u.UserAssignEventRoleService.AddUserEventRole(userId, eventRoleId, eventId, organizationId)
}

func (u *UserAssignEventRoleFacade) GetEventMembersByEventId(tokenStr string, eventId uint32) (*auth.GetEventOperationLeadersResponse, error) {
	userAssignEventRoles, err := u.UserAssignEventRoleService.GetEventMembersByEventId(eventId)
	if err != nil {
		return nil, err
	}
	res := populator.GetEventOperationLeadersPopulator(userAssignEventRoles)
	return res, nil
}

func (u *UserAssignEventRoleFacade) GetEventsByToken(tokenStr string) (*auth.GetEventsByUserIdResponse, error) {
	events, err := u.UserAssignEventRoleService.GetEventsByAccessToken(tokenStr)
	if err != nil {
		return nil, err
	}
	var res auth.GetEventsByUserIdResponse
	for _, event := range *events {
		id := uint32(event.EventId)
		role := uint32(event.EventRoleId)
		res.Events = append(res.Events, &auth.GetEventsByUserIdDict{EventId: &id, EventRole: &role})
	}
	return &res, nil
}


