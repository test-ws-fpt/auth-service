package impl

import (
	"ems/auth-service/repository"
	"ems/auth-service/service"
	"github.com/google/wire"
)


var _ service.IEventRolePermissionService = (*EventRolePermissionService)(nil)

var EventRolePermissionServiceSet = wire.NewSet(wire.Struct(new(EventRolePermissionService), "*"), wire.Bind(new(service.IEventRolePermissionService), new(*EventRolePermissionService)))

type EventRolePermissionService struct {
	EventRolePermissionRepository repository.IEventRolePermissionRepository
	UserAssignEventRoleRepository repository.IUserAssignEventRoleRepository
}

func (e *EventRolePermissionService) CheckEventRolePermission(tokenStr string, PermissionId, eventId uint32) (bool, error) {
	userId, err := getUserIdFromAccessToken(tokenStr)
	if err != nil {
		return false, err
	}
	userAssignEventRole, err := e.UserAssignEventRoleRepository.GetUserEventRoleByUserIdAndEventId(userId, uint(eventId))
	if err != nil {
		return false, err
	}
	return e.EventRolePermissionRepository.CheckEventRolePermission(userAssignEventRole.EventRoleId, uint(PermissionId))
}
