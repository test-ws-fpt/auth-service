module ems/auth-service

go 1.14

// This can be removed once etcd becomes go gettable, version 3.4 and 3.5 is not,
// see https://github.com/etcd-io/etcd/issues/11154 and https://github.com/etcd-io/etcd/issues/11931.
replace google.golang.org/grpc => google.golang.org/grpc v1.26.0

replace ems/shared => bitbucket.org/test-ws-fpt/shared v1.1.4

require (
	ems/shared v1.1.4
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/google/wire v0.4.0
	github.com/jinzhu/gorm v1.9.16
	github.com/micro/go-micro/v2 v2.9.1
	github.com/micro/go-plugins/broker/rabbitmq/v2 v2.9.1
	golang.org/x/crypto v0.0.0-20200510223506-06a226fb4e37
)
