// +build wireinject


package injector

import (
	"github.com/google/wire"
	facade "ems/auth-service/facade/impl"
	repository "ems/auth-service/repository/mysql"
	service "ems/auth-service/service/impl"
)

func BuildInjector() (*Injector, func(), error) {
	wire.Build(
		InitGormDB,
		repository.RepositorySet,
		service.ServiceSet,
		facade.FacadeSet,
		InjectorSet,
	)
	return new(Injector), nil, nil
}