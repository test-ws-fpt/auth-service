package impl

import (
	"ems/auth-service/facade"
	"ems/auth-service/service"
	"github.com/google/wire"
)

type EventRolePermissionFacade struct {
	EventRolePermissionService service.IEventRolePermissionService
}


var _ facade.IEventRolePermissionFacade = (*EventRolePermissionFacade)(nil)

var EventRolePermissionFacadeSet = wire.NewSet(wire.Struct(new(EventRolePermissionFacade), "*"), wire.Bind(new(facade.IEventRolePermissionFacade), new(*EventRolePermissionFacade)))

func (e *EventRolePermissionFacade) CheckEventRolePermission(tokenStr string, PermissionId, eventId uint32) (bool, error) {
	return e.EventRolePermissionService.CheckEventRolePermission(tokenStr, PermissionId, eventId)
}
