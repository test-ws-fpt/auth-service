package impl

import (
	"ems/auth-service/repository"
	"ems/auth-service/service"
	utils "ems/auth-service/utils"
	emsError "ems/shared/error"
	"errors"
	"fmt"
	"github.com/google/wire"
	log "github.com/micro/go-micro/v2/logger"
	"strconv"
)

var _ service.IRolePermissionService = (*RolePermissionService)(nil)

var RolePermissionServiceSet = wire.NewSet(wire.Struct(new(RolePermissionService), "*"), wire.Bind(new(service.IRolePermissionService), new(*RolePermissionService)))

type RolePermissionService struct {
	RolePermissionRepository repository.IRolePermissionRepository
	UserAssignRoleRepository repository.IUserAssignRoleRepository
}

func (e *RolePermissionService) CheckRolePermission(tokenStr string, PermissionId uint32) (bool, error) {
	userId, err := getUserIdFromAccessToken(tokenStr)
	if err != nil {
		return false, err
	}
	userAssignRole, err := e.UserAssignRoleRepository.GetUserRoleByUserId(userId)
	if err != nil {
		return false, err
	}
	return e.RolePermissionRepository.CheckRolePermission(userAssignRole.RoleId, uint(PermissionId))
}

func getUserIdFromAccessToken(tokenStr string) (uint, error) {
	token, err := utils.ParseToken(tokenStr)
	if err != nil {
		log.Info(err.Error())
		return 0, &emsError.ResultError{ErrorCode: 401, Err: errors.New("invalid access token")}
	}
	claims, err := utils.GetAccessTokenClaims(token)
	if err != nil {
		log.Info(err.Error())
		return 0, &emsError.ResultError{ErrorCode: 401, Err: errors.New("invalid access token")}
	}
	userId, err := strconv.ParseUint(fmt.Sprint(claims["userId"]), 10, 32)
	if err != nil {
		log.Info(err.Error())
		return 0, &emsError.ResultError{ErrorCode: 401, Err: errors.New("invalid access token")}
	}
	return uint(userId), nil
}
