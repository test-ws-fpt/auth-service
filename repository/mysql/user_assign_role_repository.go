package mysql

import (
	"ems/auth-service/model"
	repo "ems/auth-service/repository"
	emsError "ems/shared/error"
	"errors"
	"github.com/google/wire"
	"github.com/jinzhu/gorm"
	log "github.com/micro/go-micro/v2/logger"
)

var _ repo.IUserAssignRoleRepository = (*UserAssignRoleRepository)(nil)

var UserAssignRoleRepositorySet = wire.NewSet(wire.Struct(new(UserAssignRoleRepository), "*"), wire.Bind(new(repo.IUserAssignRoleRepository), new(*UserAssignRoleRepository)))

type UserAssignRoleRepository struct {
	DB *gorm.DB
}


func (u *UserAssignRoleRepository) GetEventRunners(organizationId uint) (*[]model.UserAssignRole, error) {
	var userAssignRoles []model.UserAssignRole
	if res := u.DB.Preload("User", "organization_id = ?", organizationId).Where("is_active = ? AND role_id = ?", 1, 3).Find(&userAssignRoles); res.Error != nil {
		if !gorm.IsRecordNotFoundError(res.Error) {
			log.Info(res.Error.Error())
			return nil, &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when getting event runners")}
		}
	}
	return &userAssignRoles, nil
}

func (u *UserAssignRoleRepository) GetUserRoleByUserId(userId uint) (*model.UserAssignRole, error) {
	userAssignRole := model.UserAssignRole{}
	if res := u.DB.Where("user_id = ? AND is_active = 1", userId).First(&userAssignRole); res.Error != nil {
		log.Info(res.Error.Error())
		if gorm.IsRecordNotFoundError(res.Error) {
			return nil, &emsError.ResultError{ErrorCode: 400, Err: errors.New("user does not have any role")}
		} else {
			return nil, &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when getting user role")}
		}
	}
	return &userAssignRole, nil
}

func (u *UserAssignRoleRepository) AddUserRole(userAssignRole *model.UserAssignRole) (*model.UserAssignRole, error) {
	res := u.DB.Create(userAssignRole)
	if res.Error != nil {
		log.Info(res.Error.Error())
		return nil, &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when adding getting user role")}
	}
	return u.GetUserRoleByUserId(userAssignRole.UserId)
}

