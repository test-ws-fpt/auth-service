package utils

import (
	"errors"
	"github.com/dgrijalva/jwt-go"
)

// get information stored in access token
func GetAccessTokenClaims(token *jwt.Token) (jwt.MapClaims, error) {
	if claims, ok := token.Claims.(jwt.MapClaims); ok {
		return claims, nil
	} else {
		return nil, errors.New("Failed to get token claims!")
	}
}
