package model

type EventRoleGroup struct {
	ID       	uint `gorm:"primary_key;auto_increment"`
	Name    		string
	OrganizationId  uint
	Organization 	Organization
	Description 	string

}

func (EventRoleGroup) TableName() string {
	return "event_role_group"
}

