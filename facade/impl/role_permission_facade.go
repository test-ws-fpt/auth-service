package impl

import (
	"ems/auth-service/facade"
	"ems/auth-service/service"
	"github.com/google/wire"
)

type RolePermissionFacade struct {
	RolePermissionService service.IRolePermissionService
}


var _ facade.IRolePermissionFacade = (*RolePermissionFacade)(nil)

var RolePermissionFacadeSet = wire.NewSet(wire.Struct(new(RolePermissionFacade), "*"), wire.Bind(new(facade.IRolePermissionFacade), new(*RolePermissionFacade)))

func (e *RolePermissionFacade) CheckRolePermission(tokenStr string, PermissionId uint32) (bool, error) {
	return e.RolePermissionService.CheckRolePermission(tokenStr, PermissionId)
}
