package utils

import (
	"github.com/dgrijalva/jwt-go"
	"io/ioutil"
)

//parse access token, return token model if valid token, error otherwise
func ParseToken(tokenStr string) (*jwt.Token, error) {
	urlPublicKey := "key/id.rsa.pub"
	keyData, err := ioutil.ReadFile(urlPublicKey)
	if err != nil {
		return nil, err
	}
	key, err := jwt.ParseRSAPublicKeyFromPEM(keyData)
	if err != nil {
		return nil, err
	}
	token, err := jwt.Parse(tokenStr, func(token *jwt.Token) (interface{}, error) {
		return key, nil
	})

	if err != nil {
		return nil, err
	} else {
		return token, nil
	}

}