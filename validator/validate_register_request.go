package validator

import (
	emsError "ems/shared/error"
	auth "ems/shared/proto/auth"
	"errors"
	"regexp"
	"strings"
)

func ValidateRegisterRequest(req *auth.ValidateRegisterRequest) error {
	re := regexp.MustCompile(`^0[0-9]{9}$`)
	if strings.TrimSpace(*req.Username) == "" {
		return &emsError.ResultError{ErrorCode: 400, Err: errors.New("invalid username")}
	}
	if !re.MatchString(*req.Phone)  {
		return &emsError.ResultError{ErrorCode: 400, Err: errors.New("invalid phone number")}
	}
	at := strings.LastIndex(*req.Email, "@")
	if at < 0 {
		return &emsError.ResultError{ErrorCode: 400, Err: errors.New("invalid email address")}
	}

	return nil
}