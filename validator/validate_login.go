package validator

import (
	emsError "ems/shared/error"
	auth "ems/shared/proto/auth"
	"errors"
	"strings"
)

func ValidateLogin(req *auth.LoginRequest) error {
	if strings.TrimSpace(*req.Username) == "" {
		return &emsError.ResultError{ErrorCode: 400, Err: errors.New("invalid username")}
	}
	if strings.TrimSpace(*req.Password) == "" {
		return &emsError.ResultError{ErrorCode: 400, Err: errors.New("invalid password")}
	}

	return nil
}