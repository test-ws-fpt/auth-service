package model

type EventRolePermission struct {
	ID       	uint `gorm:"primary_key;auto_increment"`
	PermissionId 	uint
	Permission 		Permission
	EventRoleId 	uint
	EventRole 		EventRole

}

func (EventRolePermission) TableName() string {
	return "event_role_permission"
}