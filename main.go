package main

import (
	"ems/auth-service/handler"
	"ems/auth-service/injector"
	otpPB "ems/shared/proto/otp"
	"github.com/micro/go-micro/v2"
	log "github.com/micro/go-micro/v2/logger"
	_ "github.com/micro/go-plugins/broker/rabbitmq/v2"
)

func main() {
	// New Service
	service := micro.NewService(
		micro.Name("go.micro.service.auth"),
		micro.Version("latest"),
	)

	// Initialise service
	service.Init()
	otpSrvClient := otpPB.NewOtpService("go.micro.service.otp", service.Client())
	injector, _, _ := injector.BuildInjector()

	// Register Handler
	handler.Apply1(service.Server(), *injector, otpSrvClient)

	// Register Struct as Subscriber

	// Run service
	if err := service.Run(); err != nil {
		log.Fatal(err)
	}
}
